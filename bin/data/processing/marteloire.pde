///////////////////////////////////////////////
// from Beesandbombs
int[][] result;
float t, c;

boolean recording = true;

int samplesPerFrame = 1;
int numFrames = 80;
//int numFrames = 4;        
float shutterAngle = 1;


float ease(float p) {
  return 3*p*p - 2*p*p*p;
}

float ease(float p, float g) {
  if (p < 0.5) 
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}

float easeAR(float p, float g){
  if (p < 0.5){
    return ease(p*2, g);
  }else{
    return ease(1-((t-0.5)*2), g);
  }
}


void draw() {
  
  if (!recording) {
    t = mouseX*1.0/width;
    c = mouseY*1.0/height;
    if (mousePressed)
      println(c);
    draw_();
  } else {
    for (int i=0; i<width*height; i++)
      for (int a=0; a<3; a++)
        result[i][a] = 0;
    
    c = 0;
    for (int sa=0; sa<samplesPerFrame; sa++) {
      t = map(frameCount-1 + sa*shutterAngle/samplesPerFrame, 0, numFrames, 0, 1);
      //      println(frameCount-1 + sa*shutterAngle/samplesPerFrame)
      draw_();
      loadPixels();
      for (int i=0; i<pixels.length; i++) {
        result[i][0] += pixels[i] >> 16 & 0xff;
        result[i][1] += pixels[i] >> 8 & 0xff;
        result[i][2] += pixels[i] & 0xff;
      }
    }
    
    loadPixels();
    for (int i=0; i<pixels.length; i++)
      pixels[i] = 0xff << 24 | 
        int(result[i][0]*1.0/samplesPerFrame) << 16 | 
        int(result[i][1]*1.0/samplesPerFrame) << 8 | 
        int(result[i][2]*1.0/samplesPerFrame);
    updatePixels();

    saveFrame("f###.gif");

    //if (frameCount==1)
    if (frameCount==numFrames)
      exit();
  }
}

////////////////////////////////////////////////////

float h;
float w;


int n_cross = 3;

void setup(){
  size(1000, 1000);
  blendMode(32);
  smooth(8);
  result = new int[width*height][3];
  print(TWO_PI);
}

void draw_(){
  background(254);
  pushMatrix();
  translate(width/2, height/2);
  rotate(-PI/2);
  //rotate(PI*t/(n_cross/2));
  w = width;
  h = height;
  
  //  stroke(255);
  //  strokeWeight(20);
  //  drawGrid(100, 4);
  noStroke();
  println(ease(t)*w/4);



  float rot_step;
  float in_rot_step;
  float rot_start = 0;

  //  float tt = map(mouseY, 0, height, 0, 500);;
  float tt = 250;
  // if (t<0.5){
  //   rot_step = TWO_PI*ease(t*2)/n_cross;
  //   //tt = ease(t*2)*400;
  //   //  float in_rot_step = -rot_step; // -TWO_PI/n_cross

  // }else{
  //   rot_start = TWO_PI;
  //   rot_step = -TWO_PI*ease(1-((t-0.5)*2))/n_cross;
  //   //tt = ease(1-((t-0.5)*2))*400;
  //   //    rot_step = TWO_PI*ease(1-t)/n_cross;
  // }

  rot_step = TWO_PI/n_cross;
  in_rot_step = -TWO_PI/n_cross;  
  
  if (t<0.5){
    rot_step = TWO_PI*ease(t*2, 4)/n_cross;
    //tt = ease(t*2)*400;
    //  float in_rot_step = -rot_step; // -TWO_PI/n_cross

  }else{
    rot_start = TWO_PI;
    rot_step = -TWO_PI*ease(1-((t-0.5)*2), 3)/n_cross;
    //tt = ease(1-((t-0.5)*2))*400;
    //    rot_step = TWO_PI*ease(1-t)/n_cross;
  }
  
  //  drawCross(n_cross);

  // for (int i = 0; i < n_cross; i++){

  //   pushMatrix();
  //   rotate(rot_start + rot_step*i);
  //   translate(tt, 0);
  //   rotate(in_rot_step*i);
  //   drawCross(n_cross, 2);
  //   popMatrix();
  // }
  
  // for (int i = 0; i < n_cross; i++){
    
  //   pushMatrix();
  //   rotate(rot_start + rot_step*i);
  //   translate(tt, 0);
  //   rotate(in_rot_step*i);
  //   drawCross(n_cross, 1);
  //   popMatrix();
  // }


  //  drawCrossSurf(n_cross+1, 0);
  
  for (int i = 0; i < n_cross; i++){
    
    pushMatrix();
    rotate(rot_start + rot_step*i);
    translate(tt, 0);
    rotate(in_rot_step*i);
    //    drawCross(n_cross, 0);
    drawCrossSurf(n_cross, 0);
    popMatrix();
  }
  
  // if (t>0.0){
  //   drawCrossL(n_cross, map(t, 0.0, 1, w, 0));
  // }
  popMatrix();
}

void drawGrid(int step, int n){
  for (int i = -n ; i < n+1 ; i++){
    line(-w/2, i*step, w*3/4, i*step);
    line(i*step, -h/2, i*step, h/2);
  }
}

void drawCrossSurf(int n, int b){
  pushMatrix();
  float w = width;
  for (int i = 1; i < n+1; i++){
    rotate(TWO_PI/n);
    if (b%2 == 1){
      triangle(0, 0, w, 0, w, w*tan(TWO_PI/(2*n)));
    }else{
      triangle(0, 0, w, 0, w, -w*tan(TWO_PI/(2*n)));
    }
  }
  popMatrix();
}

void drawCross(int n, int c){
  pushMatrix();
  boolean d = false;

  if (c == -1){
    d = true;
  }
  for (int i = 1; i < n+1; i++){
     if (i%4 == 0){
       stroke(255);
       if (c == 0){
         d = true;
       }
       strokeWeight(10);
     }else if (i%2 == 0){
       if (c == 1){
         d = true;
       }
       stroke(255);
       strokeWeight(5);
     }else{
       if (c == 2){
         d = true;
       }
       strokeWeight(2);
       stroke(255);
     }
    float l = w*6;
    // if (i%2 == 0){
    //   stroke(255);
    // }else{
    //   stroke(200, 0, 0);
    //   l = w;
    // }
    rotate(PI/n);
    // if (t < 0.5){
    //   rotate(PI*easeAR(t, 4)/n);
    // }else{
    //   rotate(-PI*(easeAR(t, 4))/n);
    // }
     if(d == true){
       if (c == 2){
         dashedLine(l);
       }else{
            
         line(-l, 0, l, 0);
       }
     }
     
     d = false;
  }
  popMatrix();
}

void dashedLine(float l){
  int e = 8;
  for (int i = 0; i < l; i = i+2*e){
    line(i, 0, i+e, 0);
    line(-i, 0, -i+e, 0);
  }
}

void drawCrossL(int n, float l){
  pushMatrix();
  for (int i = 1; i < n+1; i++){
    // if (i%4 == 0){
    //   stroke(255);
    // }else if (i%2 == 0){
    //   stroke(10);
    // }else{
    //   stroke(90);
    // }
    stroke(255);
    if (i%2 == 0){
      stroke(255);
    }else{
      stroke(200, 0, 0);
    }

    rotate(PI/n);
    line(-w, 0, -l, 0);
    line(w, 0, l, 0);
  }
  popMatrix();
}


void keyPressed(){
  n_cross += 1;
}

void mousePressed(){
  n_cross -= 1;
}
