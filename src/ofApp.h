// =============================================================================
//
// Copyright (c) 2010-2014 Christopher Baker <http://christopherbaker.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// =============================================================================


#pragma once


#include "ofMain.h"
#include "ofxGui.h"
#include "ofxOscParameterSync.h"
#include "ofxEasing.h"
#include "ofxClipper.h"

class ofApp: public ofBaseApp
{
public:

    void setup();
    void update();
    void draw();

    void mouseMoved(int x, int y);
    void keyPressed(int key);
  
    ofPolyline transform(ofPolyline& line, glm::vec3 pos, glm::vec3 pt, float angle);
    std::vector<ofPolyline> marteloire(glm::vec3 pos, int i);

    float moveRad(int i);
    
    ofx::Clipper clipper;
    
    std::vector<ofPolyline> clipSubjects;
    std::vector<ofPolyline> clipMasks;
    
    std::vector<ofPolyline> clips;

    // record utils/parameters

    ofFbo fbo;
    ofTexture fbo_tex;
    int fbo_width, fbo_height;

    int rec_tot;

    bool oneShot;
    
    // gui

    ofParameterGroup parameters;
    
    ofxOscParameterSync sync;
    
    ofxPanel gui;

    ofParameter<bool> draw_gui;
    ofParameter<bool> b_record;
    ofParameter<int> rec_cpt;

    ofParameter<int> param1_animate_mode;
    ofParameter<int> param2_animate_mode;
    
    ofParameter<float> param1;
    ofParameter<float> param2;

    ofParameter<bool> mode_fill;
    ofParameter<bool> mode_stroke;

    ofParameter<bool> mode_clock;
    ofParameter<bool> mode_spin;

    ofParameter<float> line_width;
    
    ofParameter<int> n_branches;
    ofParameter<int> n_stars;
    ofParameter<int> n_rot;

    ofParameter<float> branch_size;
    ofParameter<float> h_lines;
    ofParameter<float> h_min;
};
