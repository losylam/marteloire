// =============================================================================
//
// Copyright (c) 2010-2016 Christopher Baker <http://christopherbaker.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// =============================================================================


#include "ofApp.h"

void ofApp::setup()
{
   ofSetFrameRate(20);
    ofEnableSmoothing();
    ofEnableAntiAliasing();
  //  ofToggleFullscreen();
  ofSetFullscreen(true);
  // clipSubjects.push_back(r2);

  fbo_width = 1080;
  fbo_height = 1080;
  
  fbo.allocate(fbo_width, fbo_height, GL_RGB);
  fbo_tex.allocate(fbo_width, fbo_height, GL_RGB);

  fbo_tex = fbo.getTextureReference();

  rec_tot = 160;

  oneShot = false;
  
  parameters.setName("marteloire");

  parameters.add(draw_gui.set("draw gui", true));
  
  parameters.add(b_record.set("record", false));
  parameters.add(rec_cpt.set("rec_cpt", 0, 0, rec_tot)); 

  parameters.add(param1_animate_mode.set("p1 anim mode", 4, 0, 4));
  parameters.add(param2_animate_mode.set("p2 anim mode", 4, 0, 4));

  parameters.add(param1.set("p1", 0.783, 0.0, 1.0));
  parameters.add(param2.set("p2", 0.5, 0.0, 1.0));

  parameters.add(mode_stroke.set("stroke", true));
  parameters.add(mode_fill.set("fill", false));

  parameters.add(line_width.set("line_width", 2.0, 0.1, 10.0));

  parameters.add(mode_clock.set("clock", false));
  parameters.add(mode_spin.set("spin", false));

  parameters.add(n_stars.set("n_stars", 8, 0, 32));
  parameters.add(n_rot.set("n_rot", 4, 0, 32));
  parameters.add(h_lines.set("h_lines", 10.0, 0.0, 200.0));
  parameters.add(h_min.set("h_min", 2.0, 0.0, 100.0));

  parameters.add(n_branches.set("n_branches", 16, 0, 32));

  parameters.add(branch_size.set("branch_size", 5, 0.01, 2)); 
  
  gui.setup(parameters);
}

ofPolyline ofApp::transform(ofPolyline& line, glm::vec3 pos, glm::vec3 pt, float angle){
  ofPolyline r2;
  vector<glm::vec3> verts = line.getVertices();
  glm::mat4 mat_rot = glm::rotate(angle, pt);
  vector<glm::vec3>::iterator iter;
  for (iter = verts.begin(); iter != verts.end(); iter++){
    glm::vec4 v = glm::vec4(*iter, 0.);
    r2.addVertex(pos +  mat_rot * v);
  }
  r2.close();
  return r2;
}

std::vector<ofPolyline> ofApp::marteloire(glm::vec3 pos, int i_star){

  std::vector<ofPolyline> new_clips;
  clipMasks.clear();
  clipSubjects.clear();

  float h_min_loc = h_min * 5;
  float h;

  float angle_start = - i_star * TWO_PI * param2/n_stars;
  
  if (mode_clock){
    //  float h = 1 + param2 * 100 * 5;
    if (i_star>=0){
      h = h_min_loc + h_lines * 5 * (1+ cos( (rec_cpt*TWO_PI)/rec_tot + TWO_PI*i_star/n_rot))/2;
    }else{
      h = h_min_loc;
    }
  }else{
        h = h_min_loc + h_lines * 5;

  }
  float w = ofGetWidth()*branch_size*10;
 
  //  clipMasks.push_back(r);
  
  for (int i = 0; i < n_branches; i ++){
    ofSetBackgroundColor(0);


    if (mode_spin){
      //  float h = 1 + param2 * 100 * 5;
      if (i>=0){
	h = h_min_loc + h_lines * 5 * (1+ cos( (rec_cpt*TWO_PI)/rec_tot + TWO_PI*i_star/n_rot))/2;
      }else{
	h = h_min_loc;
      }
    }else{
      //      h = h_min_loc + h_lines * 5;
      h = h_min_loc + h_lines * 5;
    }
    
    float hh = h;
    // if (i%2==1){
    //   hh = h/2;
    // }
    ofPolyline r;
    r.addVertex(glm::vec3(hh/2, w, 0));
    r.addVertex(glm::vec3(hh/2, -w, 0));
    r.addVertex(glm::vec3(-hh/2, -w, 0));
    r.addVertex(glm::vec3(-hh/2, w, 0));
    r.close();
    
    clipper.Clear();
    clipper.addPolylines(new_clips, ClipperLib::ptClip);
    
    ofPolyline r2 = transform(r, pos, glm::vec3(0, 0, 1), angle_start + PI*i/n_branches);
    clipSubjects.push_back(r2 );
    clipper.addPolyline(r2, ClipperLib::ptSubject);
    new_clips = clipper.getClipped(ClipperLib::ctUnion);
  }
  
  return new_clips;
}

float ofApp::moveRad(int i){
  int rec_div2 = rec_tot/2;

  float cpt_rad = rec_div2 / n_rot;
  
  float min_rec_1 = cpt_rad * i;
  float max_rec_1 = cpt_rad * (i + 4);

  float min_rec_2 = cpt_rad * i + rec_div2;
  float max_rec_2 = cpt_rad * (i+1) + rec_div2;;

  float fact_rad;

  if (rec_cpt < rec_div2){
    if (rec_cpt < min_rec_1){
      fact_rad = 4.0;
    }else if(rec_cpt > max_rec_1){
      fact_rad = 1.0;
    }else{
      fact_rad = ofxeasing::map(rec_cpt - min_rec_1, 0, cpt_rad*4, 3.0, 1.0, ofxeasing::cubic::easeInOut);
    }
  }else{
    
    if (rec_cpt < min_rec_2){
      fact_rad = 1.0;
    }else if(rec_cpt > max_rec_2){
      fact_rad = 0.0;
    }else{
      fact_rad = ofxeasing::map(rec_cpt - min_rec_2, 0, cpt_rad, 1.0, 0.0, ofxeasing::cubic::easeInOut);

    }
  }    
  ofLog() << ofToString(fact_rad) << " " << rec_cpt << "  " << min_rec_2;  
  return fact_rad;
}

void ofApp::update()
{

  switch(param1_animate_mode){
  case 0:
    param1 = ofMap(mouseX, 0.0, ofGetWidth(), 0.0, 1.0);
    break;
  case 1:
    param1 = float(rec_cpt) / rec_tot;
    break;
  case 2:
    if (rec_cpt < rec_tot/2){
      param1 = ofxeasing::map(rec_cpt, 0, rec_tot/2, 0.785, 1.0, ofxeasing::cubic::easeInOut);
    }else{
      param1 = ofxeasing::map(rec_cpt, rec_tot/2, rec_tot, 0.0 , 0.785, ofxeasing::cubic::easeOut);
    }
    break;
  case 3:
    param1 = ofxeasing::map(rec_cpt, 0, rec_tot, 0.0, 1.0, ofxeasing::cubic::easeOut);
    break;
  }

  switch(param2_animate_mode){
  case 0:
    param2 = ofMap(mouseY, 0.0, ofGetWidth(), 0.0, 1.0);
    break;
  case 1:
    param2 = float(rec_cpt) / rec_tot;
    break;
  case 2:
    if (rec_cpt < rec_tot/2){
      param2 = ofxeasing::map(rec_cpt, 0, rec_tot/2, 0.0, 1.0, ofxeasing::cubic::easeInOut);
    }else{
      param2 = ofxeasing::map(rec_cpt, rec_tot/2, rec_tot, 1.0, 0.0, ofxeasing::cubic::easeInOut);
    }
    break;
  case 3:
    param2 = ofxeasing::map(rec_cpt, 0, rec_tot, 0.0, 1.0, ofxeasing::cubic::easeInOut);
  }
  
  clipMasks.clear();
  clipSubjects.clear();
    
  clips.clear();
  //  clipMasks.push_back(r);

  //  clips = marteloire(glm::vec3(0, 0, 0), -1);

  // branch_size = 0.1 + param2 * 0.5;
  
  float rad = (2500 - param1 * 2500) * 10 * param2;
  for (int i = 0; i < n_stars; i ++){

    rad = rad;// * moveRad(i);
    
    clipper.Clear();
    glm::vec3 pos = glm::vec3(rad * sin(i*TWO_PI*param2/n_stars), rad * cos(i*TWO_PI*param2/n_stars), 0);
    if (i == 0){
      clips = marteloire(pos, i);

    }else{
      if (clips.size() > 0){
	
	std::vector<ofPolyline> m = marteloire(pos, i);
	
	clipper.Clear();
	clipper.addPolylines(clips, ClipperLib::ptSubject);
	
	clipper.addPolylines(m, ClipperLib::ptClip);
	
	clips = clipper.getClipped(ClipperLib::ctUnion);
      }
    }
  }

  fbo.begin();

  if (oneShot){
    ofBeginSaveScreenAsPDF("screenshot-"+ofGetTimestampString()+".pdf", false);
  }
  
  ofBackground(0);
  
  ofPushMatrix();
  ofTranslate(fbo_width/2, fbo_height/2);
  ofScale(1/10., 1/10.);

  if (mode_stroke){
  
    for (auto& clip: clips)
      {
	ofNoFill();
	ofSetLineWidth(line_width);
	clip.draw();
	
	
	// ofFill();
	// ofBeginShape();  
	// // for( int i = 0; i < clip.getVertices().size(); i++) {  
	// //   ofVertex(clip.getVertices().at(i).x, clip.getVertices().at(i).y);  
	// // }  
	
	// for( int i = 0; i < clip.getVertices().size(); i++) {  
	//   ofVertex(clip.getVertices().at(i).x, clip.getVertices().at(i).y);  
	// }  

	// ofEndShape();  
	// ofNoFill();
      }
  }
  
  if (mode_fill){
    for (int j = 1; j < clips.size(); j++){
      ofSetColor(255);
      ofBeginShape();
      ofFill();
      // for( int i = 0; i < clip.getVertices().size(); i++) {  
      //   ofVertex(clip.getVertices().at(i).x, clip.getVertices().at(i).y);  
      // }  
      
      for( int i = 0; i < clips[j].getVertices().size(); i++) {  
	ofVertex(clips[j].getVertices().at(i).x, clips[j].getVertices().at(i).y);  
      }  
      
      ofEndShape();  
      ofNoFill();
      
    }
  }
  ofPopMatrix();


  if( oneShot ){
    ofEndSaveScreenAsPDF();
    oneShot = false;
    ofLog(OF_LOG_NOTICE, "export");
  }
 
  
  fbo.end();

  if (b_record){
    ofPixels pix;
    fbo_tex.readToPixels(pix);

    char buffer[256];
    sprintf(buffer, "%03d", int(rec_cpt));
    
    ofSaveImage(pix, "capture_"+ofToString(buffer) + ".png");
  }
  
  rec_cpt = (rec_cpt+1) % rec_tot;
}


void ofApp::draw()
{

  fbo.draw((ofGetWidth()-fbo_width)/2,(ofGetHeight()-fbo_height)/2);
  
  for (auto& mask: clipMasks){
    ofSetColor(255, 0, 0);
    //    mask.draw();
  }



  if (draw_gui){
    gui.draw();
  }

 
}



void ofApp::mouseMoved(int x, int y)
{
 
}



//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  switch(key){
  case 'f':
    ofToggleFullscreen();
    break;
  case 'e':
    oneShot = true;
    break;
  }
}
